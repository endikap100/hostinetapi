#FROM python:3.9-slim
FROM python:3
#FROM ubuntu:20.04

WORKDIR /app

COPY . /app
#RUN apt-get update && \
#    DEBIAN_FRONTEND=noninteractive apt-get -qq install python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev
ENTRYPOINT [ "python", "-m", "hostinetapi" ]