import click
from time import sleep
from hostinetapi.utils import get_public_ip, resolve_dns
from hostinetapi.hostinetapi import HostinetAPI


@click.group()
@click.option('--appKey', required=True, envvar='HOSTINET_APPKEY')
@click.option('--appSecret', required=True, envvar='HOSTINET_APPSECRET')
@click.pass_context
def cli(ctx, appkey, appsecret):
    ctx.ensure_object(dict)
    ctx.obj["hostinet"] = HostinetAPI(appkey, appsecret)


@cli.command(name='dynamic_dns')
@click.option('--domain', required=True, envvar='HOSTINET_DOMAIN')
@click.option('--sub-domain', default="@", envvar='HOSTINET_SUBDOMAIN')
@click.option('--daemon', is_flag=True, envvar='HOSTINET_DAEMON', help='run the script as a deamon')
@click.option('--collect_period', type=int, default=432000, envvar='HOSTINET_COLLECT_PERIOD')  # default 12h
@click.pass_context
def dynamic_dns(ctx, domain, sub_domain, daemon, collect_period):
    while True:
        dns = domain if sub_domain == '@' else f'{sub_domain}.{domain}'
        dns_ip = resolve_dns(dns)
        public_ip = get_public_ip()
        if public_ip == str(dns_ip):
            print("Dns query doesn't have to be updated")
        else:
            print("updating the DNS")
            try:
                ctx.obj["hostinet"].update_ip(domain, sub_domain, public_ip)
                print(f'The {".".join([sub_domain, domain])} domain has been updated to {public_ip}')
            except Exception:
                print(f'failed to update the ip for {".".join([sub_domain, domain])}')
        if not daemon:
            break
        else:
            sleep(collect_period)


if __name__ == '__main__':
    cli()
