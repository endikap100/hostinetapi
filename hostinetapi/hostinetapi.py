import requests
import json


class HostinetAPI:

    def __init__(self, appKey, appSecret):
        self.apiUrl = 'https://www.hostinet.com/api/'
        self.appKey = appKey
        self.appSecret = appSecret
        self.session = requests.Session()
        self.token = self.login()

    def login(self):
        response = self.session.post(f'{self.apiUrl}auth/',
                                     data={'appkey': self.appKey, 'appsecret': self.appSecret})
        json_response = json.loads(response.text)
        if json_response['success']:
            return json_response['token']
        else:
            # TODO gesionar mala autenticacion
            pass

    def update_ip(self, domain, host, ip):
        r = self.session.post(f'{self.apiUrl}domain/dyndns/',
                              data={'domain': domain, 'host': host, 'ip': ip},
                              headers={'token': self.token})
        if not json.loads(r.text)['success']:
            raise Exception
