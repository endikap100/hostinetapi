import requests
from dns import resolver
import json


def get_public_ip():
    return json.loads(requests.get('https://api.ipify.org?format=json').text)['ip']


def resolve_dns(hostname):
    try:
        master_answer = resolver.resolve(hostname, 'A')
        return master_answer[0]
    # return None if the domain doesn't exist
    except resolver.NXDOMAIN:
        return None
